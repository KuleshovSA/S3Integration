﻿
/////////////////////////////////////////////////////////////////////////////////
// Экспортные процедуры и функции, предназначенные для использования другими
// объектами конфигурации или другими программами
/////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// ЗаписатьФайл
//  Записывает файл в хранилище.
//
// Параметры:
//  HTTPСоединение		 - HTTPСоединение					 - HTTP-соединение
//  Приемник			 - Строка							 - полное имя файла в хранилище.
//  Данные				 - ДвоичныеДанные					 - данные записываемого файла
//  ДанныеПодключения	 - Структура						 - см. Функция ПолучитьДанныеПодключения(...)
// 
// Возвращаемое значение:
//  Булево - признак успешной записи файла в хранилище.
//
Функция ЗаписатьФайл(HTTPСоединение, Приемник, Данные, ДанныеПодключения) Экспорт
	
	HTTPМетод			= "PUT";
	АдресРесурса		= Приемник;
	Содержимое			= Данные;
	
	Результат = ВыполнитьЗапросКХранилищу(
		HTTPСоединение,
		HTTPМетод,
		АдресРесурса,
		ДанныеПодключения,
		Содержимое
	);
	
	Возврат Результат;
	
КонецФункции

// СкачатьФайл
//  Получает файл из хранилища.
//
// Параметры:
//  HTTPСоединение		 - HTTPСоединение					 - установленное HTTP-соединение
//  Данные				 - Произвольный						 - в этот параметр будут помещены данные файла в виде значения типа ДвоичныеДанные.
//  Источник			 - Строка							 - полное имя файла в хранилище.
//  ДанныеПодключения	 - Структура						 - см. Функция ПолучитьДанныеПодключения(...)
// 
// Возвращаемое значение:
//  Булево - признак успешного получения файла из хранилища.
//
Функция СкачатьФайл(HTTPСоединение, Данные, Источник, ДанныеПодключения) Экспорт
	
	HTTPМетод			= "GET";
	АдресРесурса		= Источник;
	
	Ответ = Неопределено;
	
	Результат = ВыполнитьЗапросКХранилищу(
		HTTPСоединение,
		HTTPМетод,
		АдресРесурса,
		ДанныеПодключения,
		,
		Ответ
	);
	
	Если Результат Тогда	
		Данные = Ответ.ПолучитьТелоКакДвоичныеДанные();
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

// УдалитьФайл
//  Удаляет файл из хранилища.
//
// Параметры:
//  HTTPСоединение		 - HTTPСоединение					 - HTTP-соединение
//  Источник			 - Строка							 - полное имя файла в хранилище.
//  ДанныеПодключения	 - Структура						 - см. Функция ПолучитьДанныеПодключения(...)
// 
// Возвращаемое значение:
//  Булево - признак успешного удаления файла из хранилища.
//
Функция УдалитьФайл(HTTPСоединение, Источник, ДанныеПодключения) Экспорт
	
	HTTPМетод			= "DELETE";
	АдресРесурса		= Источник;
	
	Результат = ВыполнитьЗапросКХранилищу(
		HTTPСоединение,
		HTTPМетод,
		АдресРесурса,
		ДанныеПодключения
	);
	
	Возврат Результат;
	
КонецФункции

// Функция - Получить данные подключения
//
// Параметры:
//  Сервер			 - Строка								 - Адрес сервера в формате bucket-name.s3.Region.amazonaws.com
//  Регион			 - Строка								 - Регион S3
//  КлючДоступа		 - Строка								 - AWSAccessKeyId
//  СекретныйКлюч	 - Строка								 - AWSSecretKey
// 
// Возвращаемое значение:
//  Структура:
//   * Сервер
//   * ПрефиксСекретногоКлюча
//   * СекретныйКлюч
//   * КлючДоступа
//   * Регион
//   * Сервис
//   * АлгоритмШифрования
//   * СтрокаЗавершения
//
Функция ПолучитьДанныеПодключения(Сервер, Регион, КлючДоступа, СекретныйКлюч) Экспорт
	
	ДанныеПодключения = Новый Структура(
		"Сервер, ПрефиксСекретногоКлюча, СекретныйКлюч, КлючДоступа, Регион, Сервис, АлгоритмШифрования, СтрокаЗавершения",
		Сервер,
		"AWS4",
		СекретныйКлюч,
		КлючДоступа,
		Регион,
		"s3",
		"AWS4-HMAC-SHA256",
		"aws4_request"
	);
		
	Возврат ДанныеПодключения;
		
КонецФункции

// Функция - Подключится к серверу s3
//
// Параметры:
//  Сервер	 - Строка										 - Адрес сервера в формате bucket-name.s3.Region.amazonaws.com
//  Порт	 - Число										 - Номер порта S3
// 
// Возвращаемое значение:
//  HTTPСоединение - Подключение к s3
//
Функция ПодключитсяКСерверуS3(Сервер, Порт) Экспорт
	
	Возврат Новый HTTPСоединение(Сервер, Порт,,,,,Новый ЗащищенноеСоединениеOpenSSL);
	
КонецФункции

#КонецОбласти

/////////////////////////////////////////////////////////////////////////////////
// Экспортные процедуры и функции для служебного использования внутри подсистемы
/////////////////////////////////////////////////////////////////////////////////

#Область СлужебныйПрограммныйИнтерфейс

// ВыполнитьЗапросКХранилищу
//  Выполняет запрос к хранилищу.
//
// Параметры:
//  HTTPСоединение		 - HTTPСоединение					 - установленное HTTP-соединение
//  HTTPМетод			 - Строка							 - глагол HTTP-запроса.
//  АдресРесурса		 - Строка							 - адрес ресурса в запросе.
//  ДанныеПодключения	 - Структура						 - см. Функция ПолучитьДанныеПодключения(...)
//  Содержимое			 - ДвоичныеДанные					 - тело запроса.
//  Ответ				 - Произвольный						 - в этот параметр будут помещено значение с типом HTTPОтвет.
// 
// Возвращаемое значение:
//  Булево - признак успешного выполнения запроса.
//
Функция ВыполнитьЗапросКХранилищу(HTTPСоединение, HTTPМетод, АдресРесурса, ДанныеПодключения, Содержимое = "", Ответ = Неопределено) Экспорт
	
	Результат = Ложь;
	
	Заголовки = Новый Соответствие();
	Заголовки.Вставить("Content-Transfer-Encoding", "binary");
	Заголовки.Вставить("Content-Type", 				"application/octet-stream");
	
	Если HTTPМетод = "PUT" Тогда
		Заголовки.Вставить("Content-Length", 	Формат(Содержимое.Размер(), "ЧН=0; ЧГ=;"));
		Заголовки.Вставить("Expect", 			"100-continue");
	КонецЕсли;
	
	ДополнитьЗаголовки(HTTPМетод, АдресРесурса, ДанныеПодключения, Содержимое, Заголовки);
	
	HTTPЗапрос = Новый HTTPЗапрос;
	HTTPЗапрос.АдресРесурса	= АдресРесурса;
	HTTPЗапрос.Заголовки	= Заголовки;
	
	Если Содержимое <> "" Тогда
		HTTPЗапрос.УстановитьТелоИзДвоичныхДанных(Содержимое);
	КонецЕсли;
	
	Результат = ОтправитьЗапросHTTP(HTTPСоединение, HTTPМетод, HTTPЗапрос, Ответ);
	
	Если НЕ Результат 
		И ТипЗнч(Ответ) = Тип("HTTPОтвет") Тогда
		
		ЗаголовкиОтвета = "";
		
		Для каждого ЭлементКоллекции Из Ответ.Заголовки Цикл
			
			ЗаголовкиОтвета = ЗаголовкиОтвета
				+ ЭлементКоллекции.Ключ
				+ "="
				+ ЭлементКоллекции.Значение
				+ Символы.ПС;
			
		КонецЦикла;
		
		ТекстОшибки = "Ошибка выполнения запроса к S3-хранилищу:"
			+ Символы.ПС + Ответ.КодСостояния
			+ Символы.ПС + ЗаголовкиОтвета
			+ Символы.ПС + Символы.ПС + Ответ.ПолучитьТелоКакСтроку();
		
	ИначеЕсли НЕ Результат 
		И ТипЗнч(Ответ) = Тип("Строка") Тогда
		
		ТекстОшибки = Ответ;
		
	КонецЕсли;
	
	//TODO Логировать ТекстОшибки
	Если ЗначениеЗаполнено(ТекстОшибки) Тогда
		Сообщить(ТекстОшибки);
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти

/////////////////////////////////////////////////////////////////////////////////
// Процедуры и функции, составляющие внутреннюю реализацию модуля
/////////////////////////////////////////////////////////////////////////////////

#Область СлужебныеПроцедурыИФункции

Функция ОтправитьЗапросHTTP(HTTPСоединение, HTTPМетод, HTTPЗапрос, Ответ)
		
	ТекстОшибки	= "";
	
	Если HTTPСоединение = Неопределено Тогда
		Ответ = "Не установлено подключение";
		Возврат Ложь;
	КонецЕсли;

	Попытка
		Ответ = HTTPСоединение.ВызватьHTTPМетод(HTTPМетод, HTTPЗапрос);
		Возврат ОКР(Ответ.КодСостояния / 100) = 2;
	Исключение
		Ответ = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке());
	КонецПопытки;
	
	Возврат Ложь;
	
КонецФункции

Процедура ДополнитьЗаголовки(HTTPМетод, АдресРесурса, ДанныеПодключения, Содержимое, Заголовки)
	
	ДатаЗапроса = ТекущаяУниверсальнаяДата();
	
	//Сервер
	Заголовки.Вставить("Host", ДанныеПодключения.Сервер);
	
	//Дата
	Заголовки.Вставить("X-Amz-Date", Формат(ДатаЗапроса, "ДФ=yyyyMMddTHHmmssZ"));
	
	//ХешСодержимого
	ХешСодержимого = ХешSHA256(Содержимое);
	Заголовки.Вставить("X-Amz-Content-Sha256", ХешСодержимого);
	
	//Авторизация
	Полномочия = ПолучитьПолномочия(ДанныеПодключения, ДатаЗапроса);
	
	ОтсортированныеЗаголовки = ПолучитьОтсортированныеЗаголовки(Заголовки);
	ПодписываемыеЗаголовки = СтрСоединить(ОтсортированныеЗаголовки.ВыгрузитьКолонку("Ключ"), ";");
	
	КаноническийURI = ПолучитьКаноническийURI(АдресРесурса);
	КаноническаяСтрокаЗапроса = ПолучитьКаноническуюСтрокуЗапроса(АдресРесурса);
	КаноническиеЗаголовки = ПолучитьКаноническиеЗаголовки(ОтсортированныеЗаголовки);
	
	КаноническийЗапрос = СтрШаблон(
		"%1
		|%2
		|%3
		|%4
		|%5
		|%6",
		HTTPМетод,
		КаноническийURI,
		КаноническаяСтрокаЗапроса,
		КаноническиеЗаголовки + Символы.ПС,
		ПодписываемыеЗаголовки,
		ХешСодержимого
	);
	
	Подпись = ПолучитьПодпись(ДанныеПодключения, ДатаЗапроса, КаноническийЗапрос);
	
	Авторизация = ДанныеПодключения.АлгоритмШифрования
		+ " " + "Credential=" + Полномочия
		+ ", " + "SignedHeaders=" + ПодписываемыеЗаголовки
		+ ", " + "Signature=" + Подпись;
	
	Заголовки.Вставить("Authorization", Авторизация);
	
КонецПроцедуры

Функция ПолучитьПолномочия(ДанныеПодключения, ДатаЗапроса)
	
	Возврат СтрШаблон(
		"%1/%2/%3/%4/%5", 
		ДанныеПодключения.КлючДоступа, 
		Формат(ДатаЗапроса, "ДФ=yyyyMMdd"),
		ДанныеПодключения.Регион,
		ДанныеПодключения.Сервис,
		ДанныеПодключения.СтрокаЗавершения
	);
	
КонецФункции

Функция ПолучитьОтсортированныеЗаголовки(Заголовки)
	
	ЗаголовкиТаблицей = Новый ТаблицаЗначений();
	ЗаголовкиТаблицей.Колонки.Добавить("Ключ");
	ЗаголовкиТаблицей.Колонки.Добавить("Значение");
	
	Для Каждого ЭлементКоллекции Из Заголовки Цикл
		
		НоваяСтрокаТЗ = ЗаголовкиТаблицей.Добавить();
		НоваяСтрокаТЗ.Ключ		= НРег(ЭлементКоллекции.Ключ);
		НоваяСтрокаТЗ.Значение	= ЭлементКоллекции.Значение;
		
	КонецЦикла;
	
	ЗаголовкиТаблицей.Сортировать("Ключ");
	
	Возврат ЗаголовкиТаблицей;
	
КонецФункции

Функция ПолучитьКаноническийURI(АдресРесурса)
	
	ПозицияНачалаПараметровВАдресе = СтрНайти(АдресРесурса, "?");
	ПозицияКонцаКаноническогоURI = ?(ПозицияНачалаПараметровВАдресе = 0, СтрДлина(АдресРесурса), ПозицияНачалаПараметровВАдресе - 1);
	КаноническийURI = Сред(АдресРесурса, 1, ПозицияКонцаКаноническогоURI);
	Если Не СтрНачинаетсяС(КаноническийURI, "/") Тогда
		КаноническийURI = "/" + КаноническийURI;
	КонецЕсли;
	
	Возврат КодированныйURI(КаноническийURI, Ложь);
	
КонецФункции

Функция ПолучитьКаноническуюСтрокуЗапроса(АдресРесурса)
	
	ПараметрыТаблицей = Новый ТаблицаЗначений();
	ПараметрыТаблицей.Колонки.Добавить("Ключ");
	ПараметрыТаблицей.Колонки.Добавить("Значение");
	
	ПозицияНачалаПараметровВАдресе = СтрНайти(АдресРесурса, "?");
	Если ПозицияНачалаПараметровВАдресе > 0 Тогда
		
		СтрокаПараметровВАдресе = Сред(АдресРесурса, ПозицияНачалаПараметровВАдресе + 1);
		Если Не ПустаяСтрока(СтрокаПараметровВАдресе) Тогда
			
			Для Каждого ПараметрЗапроса Из СтрРазделить(СтрокаПараметровВАдресе, "&") Цикл
				
				ЧастиПараметраЗапроса = СтрРазделить(ПараметрЗапроса, "=");
				
				ИмяПараметра		= ЧастиПараметраЗапроса[0];
				ЗнечениеПараметра	= Неопределено;
				
				Если ЧастиПараметраЗапроса.Количество() > 1 Тогда
					
					ЗнечениеПараметра = ЧастиПараметраЗапроса[1];
					
				КонецЕсли;
				
				НоваяСтрокаТЗ = ПараметрыТаблицей.Добавить();
				НоваяСтрокаТЗ.Ключ		= КодированныйURI(ИмяПараметра);
				НоваяСтрокаТЗ.Значение	= КодированныйURI(ЗнечениеПараметра);
				
			КонецЦикла;
			
		КонецЕсли;
		
	КонецЕсли;
	
	ПараметрыТаблицей.Сортировать("Ключ");
	
	КаноническаяСтрокаЗапроса = "";
	Для Каждого СтрокаТЗ Из ПараметрыТаблицей Цикл
		
		СтрокаПараметра = СтрокаТЗ.Ключ + "=" + СтрокаТЗ.Значение;
		КаноническаяСтрокаЗапроса = ДобавитьСтроку(КаноническаяСтрокаЗапроса, СтрокаПараметра, "&");
		
	КонецЦикла;
	
	Возврат КаноническаяСтрокаЗапроса;
	
КонецФункции

Функция ПолучитьКаноническиеЗаголовки(ОтсортированныеЗаголовки)
	
	КаноническиеЗаголовки = "";
	
	Для Каждого СтрокаТЗ Из ОтсортированныеЗаголовки Цикл
		
		СтрокаЗаголовка = НРег(СтрокаТЗ.Ключ) + ":" + СокрЛП(СтрокаТЗ.Значение);
		КаноническиеЗаголовки = ДобавитьСтроку(КаноническиеЗаголовки, СтрокаЗаголовка, Символы.ПС);
		
	КонецЦикла;
	
	Возврат КаноническиеЗаголовки;
	
КонецФункции

Функция ПолучитьПодпись(ДанныеПодключения, ДатаЗапроса, КаноническийЗапрос)
	
	//ПодписываемыйКлюч
	Ключ = ДанныеПодключения.ПрефиксСекретногоКлюча + ДанныеПодключения.СекретныйКлюч;
	
	КлючДата				= ХешHMAC_SHA256(Ключ, 					Формат(ДатаЗапроса, "ДФ=yyyyMMdd"));
	КлючДатаРегион			= ХешHMAC_SHA256(КлючДата, 				ДанныеПодключения.Регион);
	КлючДатаРегионСервис	= ХешHMAC_SHA256(КлючДатаРегион, 		ДанныеПодключения.Сервис);
	ПодписываемыйКлюч		= ХешHMAC_SHA256(КлючДатаРегионСервис, 	ДанныеПодключения.СтрокаЗавершения);
	
	//ПодписываемаяСтрока
	ОбластьПривязки = СтрШаблон(
		"%1/%2/%3/%4",
		Формат(ДатаЗапроса, "ДФ=yyyyMMdd"),
		ДанныеПодключения.Регион,
		ДанныеПодключения.Сервис,
		ДанныеПодключения.СтрокаЗавершения
	);
	
	ХешКаноническогоЗапроса = ХешSHA256(КаноническийЗапрос);
	
	ПодписываемаяСтрока = СтрШаблон(
		"%1
		|%2
		|%3
		|%4",
		ДанныеПодключения.АлгоритмШифрования,
		Формат(ДатаЗапроса, "ДФ=yyyyMMddTHHmmssZ"),
		ОбластьПривязки,
		ХешКаноническогоЗапроса
	);
	
	Возврат СтрокаHEXИзБайтCOM(ХешHMAC_SHA256(ПодписываемыйКлюч, ПодписываемаяСтрока));;
	
КонецФункции

Функция ХешSHA256(Значение)
	
	ХешированиеДанных = Новый ХешированиеДанных(ХешФункция.SHA256);
	ХешированиеДанных.Добавить(Значение);
	
	Возврат НРег(ПолучитьHexСтрокуИзДвоичныхДанных(ХешированиеДанных.ХешСумма));
	
КонецФункции

Функция ХешHMAC_SHA256(Ключ, Строка)
	
	Попытка
		
		Текст = Новый COMОбъект("System.Text.UTF8Encoding");
		
		Криптография = Новый COMОбъект("System.Security.Cryptography.HMACSHA256");
		
		Если ТипЗнч(Ключ) = Тип("Строка") Тогда
			КлючШифрования = Текст.GetBytes_4(Ключ);
		Иначе
			КлючШифрования = Ключ;
		КонецЕсли;
		
		Криптография.Key = КлючШифрования;
		
		Возврат Криптография.ComputeHash_2(Текст.GetBytes_4(Строка));
		
	Исключение
		
		ВызватьИсключение "Ошибка генерации хеша HMAC-SHA256";
		
	КонецПопытки;
	
КонецФункции

Функция СтрокаHEXИзБайтCOM(БайтыCOM)
	
	Результат = "";
	
	Для Каждого Байт Из БайтыCOM.Выгрузить() Цикл
		
		Результат = Результат + СтрокаHEXИзДесятичногоБайта(Байт);
		
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

Функция СтрокаHEXИзДесятичногоБайта(Байт)
	
	МатрицаHEX	= "0123456789abcdef";
	Основание	= 16;
	
	СтаршийРазряд	= Цел(Байт / Основание);
	МладшийРазряд	= Байт % Основание;
	
	Результат = Сред(МатрицаHEX, СтаршийРазряд + 1, 1) + Сред(МатрицаHEX, МладшийРазряд + 1, 1);
	
	Возврат Результат;
	
КонецФункции

Функция ДобавитьСтроку(Знач ИсходнаяСтрока, Знач ДополнительнаяСтрока, Знач Разделитель = ";")
	
	Если НЕ ПустаяСтрока(ДополнительнаяСтрока) Тогда
		
		Если Не ПустаяСтрока(ИсходнаяСтрока) Тогда
			
			Возврат Строка(ИсходнаяСтрока) + Разделитель + Строка(ДополнительнаяСтрока);
			
		Иначе
			
			Возврат Строка(ДополнительнаяСтрока);
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат Строка(ИсходнаяСтрока);
	
КонецФункции

// КодированныйURI
//  Возвращает строку, закодированную по алгоритму Amazon Simple Storage Service (AWS Signature Version 4):
//  UriEncode() must enforce the following rules:
//  URI encode every byte except the unreserved characters: 'A'-'Z', 'a'-'z', '0'-'9', '-', '.', '_', and '~'.
//  The space character is a reserved character and must be encoded as "%20" (and not as "+").
//  Each URI encoded byte is formed by a '%' and the two-digit hexadecimal value of the byte.
//  Letters in the hexadecimal value must be uppercase, for example "%1A".
//  Encode the forward slash character, '/', everywhere except in the object key name.
//  For example, if the object key name is photos/Jan/sample.jpg, the forward slash in the key name is not encoded.
//
// Параметры:
//  АдресРесурса		 - Строка	 - значение URI.
//  КодироватьПрямойСлеш - Булево	 - кодировать символ "/".
// 
// Возвращаемое значение:
//  Строка - кодированный адрес ресурса.
//
Функция КодированныйURI(АдресРесурса, КодироватьПрямойСлеш = Истина)
	
	Результат = "";
	
	Для НомерСимвола = 1 По СтрДлина(АдресРесурса) Цикл
		
		КодируемыйСимвол = Сред(АдресРесурса, НомерСимвола, 1);
		
		Если КодируемыйСимвол >= "A" И КодируемыйСимвол <= "Z"
			Или КодируемыйСимвол >= "a" И КодируемыйСимвол <= "z"
			Или КодируемыйСимвол >= "0" И КодируемыйСимвол <= "9"
			Или КодируемыйСимвол = "."
			Или КодируемыйСимвол = "-"
			Или КодируемыйСимвол = "~"
			Или КодируемыйСимвол = "_"
			Или КодируемыйСимвол = "/" И Не КодироватьПрямойСлеш Тогда
			
			КодированныйСимвол = КодируемыйСимвол;
			
		Иначе
			
			КодированныйСимвол = КодироватьСтроку(КодируемыйСимвол, СпособКодированияСтроки.КодировкаURL);
			
		КонецЕсли;
		
		Результат = Результат + КодированныйСимвол;
		
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти
